package model;

public enum Sex {

    HOMME,
    FEMME;

    public String getName() {
        return name();
    }
}
