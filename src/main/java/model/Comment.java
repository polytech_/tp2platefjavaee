package model;

import javax.persistence.*;

@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    private User commentator;

    private int score = 0;

    private String comment;

    public Comment(String comment, int score, User commentator) {
        this.comment =  comment;
        this.score = score;
        this.commentator = commentator;
    }

    public Comment() {}


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getCommentator() {
        return commentator;
    }

    public void setCommentator(User commentator) {
        this.commentator = commentator;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void like() {
        score++;
    }

    public void dislike() {
        score--;
    }
}
