package model;

import dao.AssociationDao;
import dao.HibernateUtil;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Assoc {

    @Id
    private String name;
    private String activity;

    @OneToMany(mappedBy = "assoc", fetch = FetchType.LAZY)
    private List<User> members = new ArrayList<>();

    public Assoc() {}

    public Assoc(String name, String activity) {
        this.name = name;
        this.activity = activity;
    }

    public void addMember(User member) {
        members.add(member);
        member.setAssoc(this);
    }

    public List<Comment> getAllComments() {
        AssociationDao associationDao = new AssociationDao(HibernateUtil.initSession());
        List<Comment> comments = associationDao.getComments(name);
        return comments;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }
}
