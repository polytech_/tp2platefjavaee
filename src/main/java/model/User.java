package model;
import dao.HibernateUtil;
import dao.UserDao;

import javax.faces.context.FacesContext;
import javax.persistence.*;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;


@Entity
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String username;
    private String password;
    private String fullName;

    @Enumerated(EnumType.STRING)
    private Sex sex;

    @ManyToOne
    private Assoc assoc;

    @OneToMany(mappedBy = "commentator")
    private List<Comment> comments;

    public User() {}

    public User(String username, String password, String fullName, Sex sex) {
        this.username = username;
        this.password = password;
        this.fullName = fullName;
        this.sex = sex;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Assoc getAssoc() {
        return assoc;
    }

    public void setAssoc(Assoc association) {
        this.assoc = association;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void addComment(Comment comment) {
        comments.add(comment);
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }
}
