import dao.AssociationDao;
import dao.CommentDao;
import dao.HibernateUtil;
import dao.UserDao;
import model.Assoc;
import model.Comment;
import model.Sex;
import model.User;
import org.hibernate.Session;

import java.util.List;
import java.util.Optional;


/**
 * Classe de tests des requetes vers la bdd
 */
public class Test {

    public static void main(String[] args) {
//        Session hibernateSession = HibernateUtil.initSession();
//
//        AssociationDao associationDao = new AssociationDao(hibernateSession);
//        UserDao userDao = new UserDao(hibernateSession);
//        CommentDao commentDao = new CommentDao(hibernateSession);
//
//        Optional<Assoc> assos = associationDao.getAssosByName("AssosName");
//
//        if(userDao.getUsers().isEmpty()) {
//            userDao.addUser(new User("othmane97", "mdp1", "Othmane Allamou", Sex.HOMME));
//            userDao.addUser(new User("nout97", "mdp2", "Nour Nasrallah", Sex.FEMME));
//            userDao.addUser(new User("yang97", "mdp3", "Yang Yang", Sex.HOMME));
//            userDao.addUser(new User("tariq96", "mdp4", "Tariq Chaairat", Sex.HOMME));
//        }
//
//        if(!assos.isPresent()) {
//            assos = Optional.of(new Assoc("AssosName", "Random Activity"));
//            associationDao.addAssos(assos.get());
//            List<User> users = userDao.getUsers();
//            for (User user : users) {
//                associationDao.addMember(assos.get(), user);
//            }
//        }
//
//        if(commentDao.getAllComments().isEmpty()) {
//
//            Comment comment1 = new Comment("laudantium enim quasi est quidem magnam voluptate", 0, userDao.getUserByUsername("othmane97").get());
//            Comment comment2 = new Comment("est natus enim nihil est dolore omnis voluptatem numquam ", 0, userDao.getUserByUsername("nout97").get());
//            Comment comment3 = new Comment("harum non quasi et ratione ntempore iure ex voluptates ", 0, userDao.getUserByUsername("yang97").get());
//            Comment comment4 = new Comment("ut voluptatem corrupti velit nad voluptatem maiores net", 0, userDao.getUserByUsername("tariq96").get());
//            Comment comment5 = new Comment("expedita maiores dignissimos facilis nipsum est rem est fugit", 0, userDao.getUserByUsername("othmane97").get());
//            Comment comment6 = new Comment("nihil ut voluptates blanditiis autem odio dicta rerum nquisquam", 0, userDao.getUserByUsername("nout97").get());
//            Comment comment7 = new Comment("veritatis voluptates necessitatibus maiores corrupti neque", 0, userDao.getUserByUsername("yang97").get());
//            Comment comment8 = new Comment("deleniti aut sed molestias explicabo ncommodi odio ratione", 0, userDao.getUserByUsername("tariq96").get());
//
//            commentDao.addComment(comment1);
//            commentDao.addComment(comment2);
//            commentDao.addComment(comment3);
//            commentDao.addComment(comment4);
//            commentDao.addComment(comment5);
//            commentDao.addComment(comment6);
//            commentDao.addComment(comment7);
//            commentDao.addComment(comment8);
//        }


    }
}
