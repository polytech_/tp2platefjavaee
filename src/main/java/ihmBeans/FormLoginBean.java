package ihmBeans;

import dao.HibernateUtil;
import dao.UserDao;

import java.io.Serializable;


import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@ViewScoped
public class FormLoginBean implements Serializable {

    public static final String HOME_PAGE_REDIRECT = "/home.xhtml";
    public static final String LIVRET_REDIRECT = "/livretdor.xhtml";

    private String userId;
    private String userPassword;

    @Inject
    private UserBean userBean;

    public String login() {

        UserDao userDao = new UserDao(HibernateUtil.initSession());

        //On vérifie si l'utilisateur est bien présent dans la bdd
        if(userDao.getUserByCredentials(userId, userPassword).isPresent()) {
            //On set les données rentrées par l'utilisateur de l'appli dans le bean
            userBean.setCurrentUser(userDao.getUserByCredentials(userId, userPassword).get());
            //On redirige vers la page d'accueil réservée aux utilisateurs connectés
            return HOME_PAGE_REDIRECT;
        }
        //On renvoie un message d'erreur sinon
        else {
            FacesContext.getCurrentInstance().addMessage("loginForm:login",
                    new FacesMessage(FacesMessage.SEVERITY_WARN, "Connection impossible",
                            "Identifiants incorrects"));
            return null;
        }
    }

    public String getLoggedInUsername() {
        return FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
    }

    public String toLivretDor() {
        return LIVRET_REDIRECT;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }



    @Override
    public String toString() {
        return "UserBean{" +
                "userId='" + userId + '\'' +
                ", userPassword='" + userPassword + '\'' +
                '}';
    }
}
