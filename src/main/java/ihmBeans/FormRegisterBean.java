package ihmBeans;

import dao.AssociationDao;
import dao.HibernateUtil;
import dao.UserDao;
import model.Assoc;
import model.Sex;
import model.User;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;


@Named
@ViewScoped
public class FormRegisterBean implements Serializable {

    public static final String HOME_PAGE_REDIRECT = "/login.xhtml";

    private String userId;
    private String userPassword;
    private String fullName;
    private String sex;
    private String assocName;
    private List<String> sexes;

    @Inject
    AssociationBean associationBean;

    public FormRegisterBean() {

    }

    public String register() {

        UserDao userDao = new UserDao(HibernateUtil.initSession());
        AssociationDao associationDao = new AssociationDao(HibernateUtil.initSession());

        //On vérifie si l'association n'existe pas
        if(!associationDao.getAssosByName(assocName).isPresent()) {
            FacesContext.getCurrentInstance().addMessage("registerForm:fullName",
                    new FacesMessage(FacesMessage.SEVERITY_WARN, "Inscription impossible",
                            "L'association n'existe pas !"));
            return null;
        }

        //On vérifie si l'utilisateur n'existe pas
        if(!userDao.getUserByCredentials(userId, userPassword).isPresent()) {
            Assoc assoc = associationDao.getAssosByName(assocName).get();
            //On crée un nouvel utilisateur
            User userToAdd = new User(userId, userPassword, fullName, Sex.valueOf(sex));
            //On l'ajoute en tant que membre de l'association
            associationDao.addMember(assoc, userToAdd);
            //On le sauvegarde dans la bdd
            userDao.addUser(userToAdd);
            //On met à jour la liste des membres présents dans la bdd dans le bean
            associationBean.setMembers(associationDao.getMembers(assocName));
            return HOME_PAGE_REDIRECT;
        }

        else {
            FacesContext.getCurrentInstance().addMessage("registerForm:fullName",
                    new FacesMessage(FacesMessage.SEVERITY_WARN, "Inscription impossible",
                            "L'utilisateur existe déjà !"));
            return null;
        }
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAssocName() {
        return assocName;
    }

    public void setAssocName(String assocName) {
        this.assocName = assocName;
    }

    public AssociationBean getAssociationBean() {
        return associationBean;
    }

    public void setAssociationBean(AssociationBean associationBean) {
        this.associationBean = associationBean;
    }

    //Méthode servant à remplir la liste des sexes à partir de l'enum Sex
    public List<String> getSexes() {
        return Stream.of(Sex.values())
                .map(Enum::name)
                .collect(Collectors.toList());
    }


    @Override
    public String toString() {
        return "UserBean{" +
                "userId='" + userId + '\'' +
                ", userPassword='" + userPassword + '\'' +
                '}';
    }
}
