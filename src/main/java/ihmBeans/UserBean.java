package ihmBeans;


import model.User;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@SessionScoped
public class UserBean implements Serializable {

    private User currentUser;

    public static final String LOGOUT_PAGE_REDIRECT = "/homeVisitor.xhtml";

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public boolean isLoggedIn() {
        return currentUser != null;
    }

    public String logout() {
        // On supprime l'utilisateur qui était déjà connecté
        currentUser = null;
        return LOGOUT_PAGE_REDIRECT;
    }
}
