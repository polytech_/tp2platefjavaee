package ihmBeans;

import dao.AssociationDao;
import dao.CommentDao;
import dao.HibernateUtil;
import dao.UserDao;
import model.Comment;
import model.User;
import org.hibernate.Transaction;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

@Named
@SessionScoped
public class CommentBean implements Serializable {

    public static final String LIVRET_REDIRECT = "/livretdor.xhtml";

    private String usernameCommentator;

    private String commentText;

    //Liste servant à afficher les utilisateurs dans la combobox d'ajout d'un commentaire
    private List<String> users;

    @Inject
    private AssociationBean associationBean;

    public CommentBean() {
    }

    public String getUsernameCommentator() {
        return usernameCommentator;
    }

    public void setUsernameCommentator(String usernameCommentator) {
        this.usernameCommentator = usernameCommentator;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public void clearInputs(){
        setCommentText(null);
        setUsernameCommentator(null);
    }

    public List<String> getUsers() {
        return associationBean.getMembers()
                .stream()
                .map(User::getUsername).collect(Collectors.toList());
    }

    public String addComment() {
        CommentDao commentDao = new CommentDao(HibernateUtil.initSession());
        UserDao userDao = new UserDao(HibernateUtil.initSession());
        AssociationDao associationDao = new AssociationDao(HibernateUtil.initSession());

        //On récupère le commentateur
        User commentator = userDao.getUserByUsername(usernameCommentator).get();

        Transaction transaction = HibernateUtil.initSession().beginTransaction();
        //On crée le commentaire et on le sauvegarde dans la bdd
        Comment comment = new Comment(commentText, 0, commentator);
        commentDao.addComment(comment);
        transaction.commit();

        //On met à jour la liste des commentaires dans le bean
        associationBean.setComments(associationDao.getComments(associationBean.getName()));
        clearInputs();
        return LIVRET_REDIRECT;
    }
}
