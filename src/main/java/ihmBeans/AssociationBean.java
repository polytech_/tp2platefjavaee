package ihmBeans;

import dao.AssociationDao;
import dao.HibernateUtil;
import model.Comment;
import model.User;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class AssociationBean implements Serializable {

    //Dao avec lequel on va effectuer les requêtes de la bdd
    private AssociationDao associationDao = new AssociationDao(HibernateUtil.initSession());

    public static final String LIVRET_REDIRECT = "livretdor.xhtml";


    //On initialise le nom de l'association ici afin d'utiliser le dao qui se chargera de remplir les autres attributs
    private String name = "Codingsy";
    private String activity;
    private List<User> members;
    private List<Comment> comments;

    public AssociationBean() {
        //Constructeur d'initialisation des données de l'association si l'association est déjà présente
        if(associationDao.getActivityByName(name).isPresent()) {
            activity = associationDao.getActivityByName(name).get();
            members = associationDao.getMembers(name);
            comments = associationDao.getComments(name);
        }
        else {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            FacesMessage facesMessage = new FacesMessage("L'association correspondant au nom "+ name + " n'existe pas, merci de modifier l'attribut name dans le bean AssociationBean");
            facesContext.addMessage(null, facesMessage);
            name = null;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String  upVote(int idComment) {
        associationDao.voteComment(idComment, true);
        //MàJ de la liste des commentaires de l'association
        comments = associationDao.getComments(name);
        return LIVRET_REDIRECT;
    }

    public String downVote(int idComment) {
        associationDao.voteComment(idComment, false);
        //MàJ de la liste des commentaires de l'association
        comments = associationDao.getComments(name);
        return LIVRET_REDIRECT;
    }

    @Override
    public String toString() {
        return "AssociationBean{" +
                "name='" + name + '\'' +
                ", activity='" + activity +
                '}';
    }
}
