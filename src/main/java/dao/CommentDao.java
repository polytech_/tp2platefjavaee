package dao;

import model.Comment;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class CommentDao {

    private final Session hibernateSession;

    public CommentDao(Session hibernateSession) {
        this.hibernateSession = hibernateSession;
    }

    public Optional<Comment> getComment(int idComment) {
        try{
            return Optional.of(hibernateSession.find(Comment.class, idComment));
        }catch (Exception e){
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public void addComment(Comment comment) {
        Transaction transaction = hibernateSession.beginTransaction();
        hibernateSession.save(comment);
        //comment.setCommentator(user);
        transaction.commit();
    }

    public List<Comment> getAllComments() {
        try{
            return hibernateSession.createQuery("select comment from Comment comment", Comment.class)
                    .getResultList();
        }catch (Exception e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
}
