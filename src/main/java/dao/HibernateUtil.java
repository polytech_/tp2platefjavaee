package dao;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

/**
 * Classe d'initialisation de la session hibernate qui nous servira à réaliser toutes les requêtes vers la bdd
 */
public class HibernateUtil {
    public static Session initSession() {
        return new Configuration().configure().buildSessionFactory().openSession();
    }
}