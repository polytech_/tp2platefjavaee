package dao;

import model.Assoc;
import model.Comment;
import model.User;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class AssociationDao {

    private final Session hibernateSession;

    public AssociationDao(Session hibernateSession) {
        this.hibernateSession = hibernateSession;
    }

    public List<User> getMembers(String associationName) {
        try{
            return hibernateSession.createQuery("select user from User user where user.assoc.name=:name", User.class)
                    .setParameter("name", associationName)
                    .getResultList();
        }catch (Exception e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public Optional<String> getActivityByName(String associationName) {
        try{
            return hibernateSession.createQuery("select assoc.activity from Assoc assoc where assoc.name=:name", String.class)
                    .setParameter("name", associationName)
                    .getResultList()
                    .stream()
                    .findFirst();
        }catch (Exception e){
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public Optional<Assoc> getAssosByName(String associationName) {
        try{
            return hibernateSession.createQuery("select assoc from Assoc assoc where assoc.name=:name", Assoc.class)
                    .setParameter("name", associationName)
                    .getResultList()
                    .stream()
                    .findFirst();
        }catch (Exception e){
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public List<Comment> getComments(String assosName) {
        try{
            return hibernateSession.createQuery("select comment from Comment comment where comment.commentator.assoc.name=:name order by comment.score desc", Comment.class)
                    .setParameter("name", assosName)
                    .getResultList();
        }catch (Exception e){
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public void voteComment(int idComment, boolean up) {
        Transaction transaction = hibernateSession.beginTransaction();
        CommentDao commentDao = new CommentDao(hibernateSession);
        if(commentDao.getComment(idComment).isPresent()) {
            Comment comment = commentDao.getComment(idComment).get();
            int score = comment.getScore();
            if (up) {
                comment.setScore(score + 1);
            } else {
                comment.setScore(score - 1);
            }
            transaction.commit();
        }
    }

    public void addAssos(Assoc assoc) {
        Transaction transaction = hibernateSession.beginTransaction();
        hibernateSession.save(assoc);
        transaction.commit();
    }

    public void addMember(Assoc assoc, User user) {
        Transaction transaction = hibernateSession.beginTransaction();
        assoc.addMember(user);
        user.setAssoc(assoc);
        transaction.commit();
    }
}
